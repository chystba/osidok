/** @type {import('next').NextConfig} */
import bundleAnalyzer from '@next/bundle-analyzer'
const withBundleAnalyzer = bundleAnalyzer({
  enabled: process.env.ANALYZE === "true",
});

const перенаправленняВідбитНаСніпи = [
  ["/чистьба", "/chystba"],
  ["/чистьба/словник", "/chystba/slovnyk"],

  ["/рп", "/rp"],

  ["/рр", "/rr"],
  ["/рр/мережа", "/rr/merezha"],

  ["/рс/словозмінення", "/rs/slovozminennia"],
  ["/рс/истослівя", "/rs/ystoslivia"],
  ["/рс/стрій-слова", "/rs/strii-slova"],
  ["/рс/тваровий-словник", "/rs/tvarovyi-slovnyk"],
  ["/рс/товкний-словник", "/rs/tovknyi-slovnyk"],
  ["/рс/частотний-словник", "/rs/chastotnyi-slovnyk"],
  ["/рс/розбір", "/rs/rozbir"],
  ["/рс/розбір/:slovo", "/rs/rozbir/:slovo"],
];
const перенаправленняНаСніпи = перенаправленняВідбитНаСніпи.map(
  ([ua, eng]) => ({ source: encodeURI(ua), destination: eng }),
);
перенаправленняНаСніпи.push(
  ...перенаправленняВідбитНаСніпи.map(([ua, eng]) => ({
    source: ua,
    destination: eng,
  })),
);

const перенаправленняВідбитЗСніпів = перенаправленняВідбитНаСніпи.map(
  ([, eng]) => [eng, "/404"],
);
const перенаправленняЗСніпів = перенаправленняВідбитЗСніпів.map(
  ([ua, eng]) => ({ source: ua, destination: eng }),
);

export default withBundleAnalyzer({
  reactStrictMode: true,
  images: {
    domains: [],
  },
  // serverExternalPackages: ['@movnia/spilne'],
  async rewrites() {
    return {
      afterFiles: перенаправленняНаСніпи,
      beforeFiles: перенаправленняЗСніпів,
    };
  },
});

// module.exports = {
//   reactStrictMode: true,
//   swcMinify: true,
//   images: {
//     domains: [],
//   },
// }
