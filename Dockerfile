FROM node:16-alpine

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY package*.json tsconfig.json ./
COPY . .

RUN apk update

RUN npm install
RUN npm run зібрати

EXPOSE 3001

CMD [ "npm", "run", "пуск" ]