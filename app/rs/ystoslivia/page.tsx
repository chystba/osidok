"use client";

import { useEffect, useState } from "react";
import { useVyplyvaiucheVikno } from "@/складники/спільне/випливаюче-вікно";
import { useRouter, useSearchParams } from "next/navigation";
import { Box, Button, Divider, TextField, Typography } from "@mui/material";

import { статтяПоИстцюЗапит } from "./запити";
import type { СтаттяИстця, Истець } from "@movnia/ystoslivia";
import ZaholovokСторінки from "@/складники/спільне/заголовок-сторінки";

function ИстецьПервень({ истець }: { истець: Истець }) {
  return (
    <div>
      {истець.порядковеЧисло}. {истець.слово} [{истець.черпаність}] (
      {истець.порядковеЧислоВерстви})
    </div>
  );
}

export default function Holova() {
  const { ВипливаючеВікно, станВікна, назнПовідомлення } =
    useVyplyvaiucheVikno();

  const торник = useRouter();
  const відміриЗапиту = useSearchParams();

  const [слово, назнСлово] = useState<string>("");
  const [словоПоля, назнСловоПоля] = useState<string>("");
  const [статтяИстця, назнСтаттюИстця] = useState<СтаттяИстця | null>(null);

  useEffect(() => {
    (async function () {
      назнСлово(відміриЗапиту.get("слово") ?? "");
    })();
  }, [відміриЗапиту]);

  useEffect(() => {
    if (слово) назнСловоПоля(слово);
  }, [слово]);

  useEffect(() => {
    async function витяг() {
      try {
        if (слово) назнСтаттюИстця(await статтяПоИстцюЗапит(слово));
      } catch (e) {
        if (e instanceof Error) {
          назнПовідомлення(e.message);
          станВікна(true);
        } else {
          throw e;
        }
      }
    }
    витяг();
  }, [слово, назнПовідомлення, станВікна]);

  return (
    <>
      <ВипливаючеВікно />

      <ZaholovokСторінки заголовок="Истослів'я" />

      <div style={{ display: "flex" }}>
        <TextField
          id="outlined-multiline-flexible"
          label="Місце для слова..."
          value={словоПоля}
          // multiline
          maxRows={4}
          onChange={(под) => назнСловоПоля(под.target.value)}
        />
        <Button
          variant="outlined"
          onClick={(под) => {
            под.preventDefault();

            if (словоПоля) {
              const відміри = new URLSearchParams();
              відміри.set("слово", словоПоля);
              торник.push(`?${відміри.toString()}`);
            }
          }}
        >
          розібрати
        </Button>
      </div>
      <div>
        {статтяИстця ? (
          <Box>
            <Divider sx={{ pt: 1 }}>
              <Typography variant="h4">Головний Истець</Typography>
            </Divider>
            <ИстецьПервень истець={статтяИстця.головнийИстець} />
            <Divider sx={{ pt: 1 }}>
              <Typography variant="h4">Исті</Typography>
            </Divider>
            {статтяИстця.исті.map((и) => {
              return <Box key={и.исть}>- {и.исть}</Box>;
            })}
            <Divider sx={{ pt: 1 }}>
              <Typography variant="h4">Истці</Typography>
            </Divider>
            {статтяИстця.истці.map((и, вк) => {
              return <ИстецьПервень key={и.слово + вк} истець={и} />;
            })}
          </Box>
        ) : (
          <></>
        )}
      </div>
    </>
  );
}
