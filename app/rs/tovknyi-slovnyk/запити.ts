"use server";

import { вСловотовкиПоСлову } from "@movnia/tovknyi-slovnyk";

export async function вСловотовкиПоСловуЗапит(слово: string) {
  return вСловотовкиПоСлову(слово);
}
