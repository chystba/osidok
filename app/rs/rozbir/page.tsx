"use client";

import РозбірСловаПервень from "../складники/розбір-слова/розбір-слова";

export default function Holova() {
  return (
    <РозбірСловаПервень
      params={Promise.resolve({ slovo: "" })}
      коріньПосилання="/рс/розбір"
    />
  );
}
