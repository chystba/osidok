"use client";

import РозбірСловаПервень from "../../складники/розбір-слова/розбір-слова";

export default function Holova({
  params,
}: {
  params: Promise<{ slovo: string }>;
}) {
  return <РозбірСловаПервень params={params} коріньПосилання="/рс/розбір" />;
}
