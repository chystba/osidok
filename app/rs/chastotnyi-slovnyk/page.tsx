"use client";

import { useEffect, useState } from "react";
import { useVyplyvaiucheVikno } from "@/складники/спільне/випливаюче-вікно";
import { useRouter, useSearchParams } from "next/navigation";
import {
  Box,
  Button,
  Checkbox,
  Divider,
  FormControlLabel,
  Radio,
  TextField,
  Typography,
} from "@mui/material";

import { вЧастотуЗапит } from "./запити";
import ZaholovokСторінки from "@/складники/спільне/заголовок-сторінки";

export default function Holova() {
  const { ВипливаючеВікно, станВікна, назнПовідомлення } =
    useVyplyvaiucheVikno();

  const торник = useRouter();
  const відміриЗапиту = useSearchParams();

  const [слово, назнСлово] = useState<string>("");
  const [словоПоля, назнСловоПоля] = useState<string>("");
  const [чиПервинне, назнЧиПервинне] = useState<boolean>(false);
  const [чиПервиннеПоля, назнЧиПервиннеПоля] = useState<boolean>(false);
  const [частота, назнЧастота] =
    useState<Awaited<ReturnType<typeof вЧастотуЗапит>>>(null);

  useEffect(() => {
    (async function () {
      назнСлово(відміриЗапиту.get("слово") ?? "");
      назнЧиПервинне(відміриЗапиту.get("чиПервинне") === "true" ? true : false);
    })();
  }, [відміриЗапиту]);

  useEffect(() => {
    if (слово) назнСловоПоля(слово);
  }, [слово]);
  useEffect(() => {
    if (чиПервинне) назнЧиПервиннеПоля(чиПервинне);
  }, [чиПервинне]);

  useEffect(() => {
    async function витяг() {
      try {
        if (слово) назнЧастота(await вЧастотуЗапит(слово, чиПервинне));
      } catch (e) {
        if (e instanceof Error) {
          назнПовідомлення(e.message);
          станВікна(true);
        } else {
          throw e;
        }
      }
    }
    витяг();
  }, [слово, чиПервинне, назнПовідомлення, станВікна]);

  return (
    <>
      <ВипливаючеВікно />

      <ZaholovokСторінки заголовок="Товкний словник" />

      <div>
        <div style={{ display: "flex" }}>
          <TextField
            id="outlined-multiline-flexible"
            label="Місце для слова..."
            value={словоПоля}
            // multiline
            maxRows={4}
            onChange={(под) => назнСловоПоля(под.target.value)}
          />
          <Button
            variant="outlined"
            onClick={(под) => {
              под.preventDefault();

              if (словоПоля) {
                const відміри = new URLSearchParams();
                відміри.set("слово", словоПоля);
                відміри.set("чиПервинне", String(чиПервиннеПоля));
                торник.push(`?${відміри.toString()}`);
              }
            }}
          >
            розібрати
          </Button>
        </div>
        <FormControlLabel
          control={
            <Checkbox
              checked={чиПервиннеПоля}
              onChange={(под) => назнЧиПервиннеПоля(!!под.target.checked)}
            />
          }
          label="Чи первинне?"
        />
      </div>
      <div>
        {частота ? (
          <Box sx={{ mt: 2 }}>
            <p>Повна: {частота.повнаЧастота}</p>
            <p>Відносна: {частота.відноснаЧастота}</p>
          </Box>
        ) : null}
      </div>
    </>
  );
}
