"use server";

import { вЧастоту } from "@movnia/chastotnyi-slovnyk";

export async function вЧастотуЗапит(слово: string, чиПервиннеСлово: boolean) {
  return вЧастоту(слово, чиПервиннеСлово);
}
