"use client";

import { useEffect, useState } from "react";
import { useVyplyvaiucheVikno } from "@/складники/спільне/випливаюче-вікно";
import { useRouter, useSearchParams } from "next/navigation";
import { Box, Button, Divider, TextField, Typography } from "@mui/material";

import { ськатиСловотварЗапит } from "./запити";
import ZaholovokСторінки from "@/складники/спільне/заголовок-сторінки";

export default function Holova() {
  const { ВипливаючеВікно, станВікна, назнПовідомлення } =
    useVyplyvaiucheVikno();

  const торник = useRouter();
  const відміриЗапиту = useSearchParams();

  const [слово, назнСлово] = useState<string>("");
  const [словоПоля, назнСловоПоля] = useState<string>("");
  const [словотвари, назнСловотвари] = useState<
    Awaited<ReturnType<typeof ськатиСловотварЗапит>>
  >([]);

  useEffect(() => {
    (async function () {
      назнСлово(відміриЗапиту.get("слово") ?? "");
    })();
  }, [відміриЗапиту]);

  useEffect(() => {
    if (слово) назнСловоПоля(слово);
  }, [слово]);

  useEffect(() => {
    async function витяг() {
      try {
        if (слово) назнСловотвари(await ськатиСловотварЗапит(слово));
      } catch (e) {
        if (e instanceof Error) {
          назнПовідомлення(e.message);
          станВікна(true);
        } else {
          throw e;
        }
      }
    }
    витяг();
  }, [слово, назнПовідомлення, станВікна]);

  return (
    <>
      <ВипливаючеВікно />

      <ZaholovokСторінки заголовок="Тваровий словник" />

      <div style={{ display: "flex" }}>
        <TextField
          id="outlined-multiline-flexible"
          label="Місце для слова..."
          value={словоПоля}
          // multiline
          maxRows={4}
          onChange={(под) => назнСловоПоля(под.target.value)}
        />
        <Button
          variant="outlined"
          onClick={(под) => {
            под.preventDefault();

            if (словоПоля) {
              const відміри = new URLSearchParams();
              відміри.set("слово", словоПоля);
              торник.push(`?${відміри.toString()}`);
            }
          }}
        >
          розібрати
        </Button>
      </div>
      <div>
        {словотвари.map((ст, вк) => {
          return (
            <Box key={ст.слово + вк} sx={{ my: 2 }}>
              {вк !== 0 ? <Divider /> : null}
              <pre>{JSON.stringify(ст, null, 2)}</pre>
            </Box>
          );
        })}
      </div>
    </>
  );
}
