"use server";

import { ськатиСловотвар } from "@movnia/tvarovyi-slovnyk";

export async function ськатиСловотварЗапит(слово: string) {
  return ськатиСловотвар(слово);
}
