"use client";

import { useEffect, useState } from "react";
import { useVyplyvaiucheVikno } from "@/складники/спільне/випливаюче-вікно";
import { useRouter, useSearchParams } from "next/navigation";
import { Box, Button, Divider, TextField, Typography } from "@mui/material";

import { словоВСтройовіДаніЗапит } from "./запити";
import ZaholovokСторінки from "@/складники/спільне/заголовок-сторінки";

export default function Holova() {
  const { ВипливаючеВікно, станВікна, назнПовідомлення } =
    useVyplyvaiucheVikno();

  const торник = useRouter();
  const відміриЗапиту = useSearchParams();

  const [слово, назнСлово] = useState<string>("");
  const [словоПоля, назнСловоПоля] = useState<string>("");
  const [стройовіДані, назнСтройовіДані] = useState<
    Awaited<ReturnType<typeof словоВСтройовіДаніЗапит>>
  >([]);

  useEffect(() => {
    (async function () {
      назнСлово(відміриЗапиту.get("слово") ?? "");
    })();
  }, [відміриЗапиту]);

  useEffect(() => {
    if (слово) назнСловоПоля(слово);
  }, [слово]);

  useEffect(() => {
    async function витяг() {
      try {
        if (слово) назнСтройовіДані(await словоВСтройовіДаніЗапит(слово));
      } catch (e) {
        if (e instanceof Error) {
          назнПовідомлення(e.message);
          станВікна(true);
        } else {
          throw e;
        }
      }
    }
    витяг();
  }, [слово, назнПовідомлення, станВікна]);

  return (
    <>
      <ВипливаючеВікно />

      <ZaholovokСторінки заголовок="Стрій слова" />

      <div style={{ display: "flex" }}>
        <TextField
          id="outlined-multiline-flexible"
          label="Місце для слова..."
          value={словоПоля}
          // multiline
          maxRows={4}
          onChange={(под) => назнСловоПоля(под.target.value)}
        />
        <Button
          variant="outlined"
          onClick={(под) => {
            под.preventDefault();

            if (словоПоля) {
              const відміри = new URLSearchParams();
              відміри.set("слово", словоПоля);
              торник.push(`?${відміри.toString()}`);
            }
          }}
        >
          розібрати
        </Button>
      </div>
      <div>
        {стройовіДані.map(({ стрічкаСтрою, словотвар, сзВуз }, вк) => {
          return (
            <Box key={сзВуз.тезь + вк} sx={{ my: 2 }}>
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>- {стрічкаСтрою}</div>
                <div style={{ display: "flex" }}>
                  <pre style={{ maxHeight: "300px", overflow: "scroll" }}>
                    ({JSON.stringify(словотвар, null, 2)})
                  </pre>
                  <pre style={{ maxHeight: "300px", overflow: "scroll" }}>
                    ({JSON.stringify(сзВуз, null, 2)})
                  </pre>
                </div>
              </div>
            </Box>
          );
        })}
      </div>
    </>
  );
}
