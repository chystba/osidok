"use server";

import { вМногоРС } from "@movnia/rozbir-slova";

export async function витягРозбориСлова(слово: string) {
  return вМногоРС(
    {
      стрічка: слово,
      чиПершеВРеченні: false,
    } /*, { чиСькатиПоПохожих: true }*/,
  );
}
