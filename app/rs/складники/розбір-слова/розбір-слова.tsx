"use client";

import { useEffect, useState } from "react";
import "./розбір-слова.css";
import { useVyplyvaiucheVikno } from "@/складники/спільне/випливаюче-вікно";
import { ДаніСловозміни, РозбірСлова } from "@movnia/spilne";
import { увузитиПо } from "@/app/загальне/обробні";
import { useRouter } from "next/navigation";
import { Button, TextField } from "@mui/material";
import { витягРозбориСлова } from "./запити";
import ZaholovokСторінки from "@/складники/спільне/заголовок-сторінки";

function хухритиКлючіСловозміни(ключі: string[]) {
  ключі.sort((к1, к2) => (к1 === "ознаки" ? 1 : к2 === "ознаки" ? -1 : 0));
  return ключі;
}

const відбитВивідКлючівСловозміни: Record<string, string> = {
  частинаМови: "чм",
  знахіднийВид: "знахВид",
  доконаність: "док.",
  відмінок: "відм.",
};

export default function Holova({
  params,
  коріньПосилання,
}: {
  params: Promise<{ slovo: string }>;
  коріньПосилання: string;
}) {
  const { ВипливаючеВікно, станВікна, назнПовідомлення } =
    useVyplyvaiucheVikno();

  const торник = useRouter();

  const [слово, назнСлово] = useState<string>("");
  const [словоПоля, назнСловоПоля] = useState<string>("");
  const [розбориСлова, назнРозбориСлова] = useState<РозбірСлова[]>([]);

  useEffect(() => {
    (async function () {
      назнСлово(decodeURIComponent((await params).slovo));
    })();
  }, [params]);

  useEffect(() => {
    if (слово) назнСловоПоля(слово);
  }, [слово]);

  useEffect(() => {
    async function витяг() {
      try {
        if (слово) назнРозбориСлова(await витягРозбориСлова(слово));
      } catch (e) {
        if (e instanceof Error) {
          назнПовідомлення(e.message);
          станВікна(true);
        } else {
          throw e;
        }
      }
    }
    витяг();
  }, [слово, назнПовідомлення, станВікна]);

  const увузеніРозбориСлова = увузитиПо(розбориСлова, (рс) =>
    JSON.stringify(рс.тваровийРС.словотвар),
  );

  return (
    <>
      <ВипливаючеВікно />

      <ZaholovokСторінки заголовок="Розбір Слова" />

      <div style={{ display: "flex" }}>
        <TextField
          id="outlined-multiline-flexible"
          label="Місце для слова..."
          value={словоПоля}
          // multiline
          maxRows={4}
          onChange={(под) => назнСловоПоля(под.target.value)}
        />
        <Button
          variant="outlined"
          onClick={(под) => {
            под.preventDefault();

            if (словоПоля) {
              торник.push(`${коріньПосилання}/${словоПоля}`);
            }
          }}
        >
          розібрати
        </Button>
      </div>
      <div>
        {[...увузеніРозбориСлова.values()].map(([рс], вк) => {
          const словозміни = рс.словозміннийРС.сзЗВузом.вуз.словозміни;
          const увузеніПоКлючах = увузитиПо(словозміни, (сз) =>
            хухритиКлючіСловозміни(Object.keys(сз.дані)).join(","),
          );
          return (
            <div key={рс.слово + вк} style={{ margin: "0px 0px 30px 0px" }}>
              <p>{рс.слово} </p>
              <hr />
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>
                  {[...увузеніПоКлючах.entries()].map(
                    ([ключіСтр, увСловозміни]) => {
                      const ключі = ключіСтр.split(
                        ",",
                      ) as (keyof ДаніСловозміни)[];
                      return (
                        <div key={ключіСтр} className="розбір-слова-підщільник">
                          <div className="розбір-слова-рядок">
                            <div className="розбір-слова-стовпчик"></div>
                            {ключі.map((кл) => (
                              <div key={кл} className="розбір-слова-стовпчик">
                                {відбитВивідКлючівСловозміни[кл] ?? кл}
                              </div>
                            ))}
                          </div>
                          {увСловозміни.map((сз, вк2) => {
                            return (
                              <div
                                className="розбір-слова-рядок"
                                key={рс.слово + ключіСтр + вк2}
                              >
                                <div className="розбір-слова-стовпчик">
                                  {сз.слово === слово ? (
                                    <b>{сз.слово}</b>
                                  ) : (
                                    сз.слово
                                  )}
                                </div>
                                {ключі.map((кл) => (
                                  <div
                                    key={кл}
                                    className="розбір-слова-стовпчик"
                                  >
                                    {сз.дані[кл]}
                                  </div>
                                ))}
                              </div>
                            );
                          })}
                        </div>
                      );
                    },
                  )}
                </div>
                <pre style={{ maxHeight: "300px", overflow: "scroll" }}>
                  (
                  {JSON.stringify(рс.словозміннийРС.сзЗВузом.вуз.дані, null, 2)}
                  )
                </pre>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
}
