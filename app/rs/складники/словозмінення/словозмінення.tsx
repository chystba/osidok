"use client";

import { useEffect, useState } from "react";
import "./словозмінення.css";
import { useVyplyvaiucheVikno } from "@/складники/спільне/випливаюче-вікно";
import { ДаніСловозміни, СловозмінаЗВузом } from "@movnia/spilne";
import { увузитиПо } from "@/app/загальне/обробні";
import { useRouter, useSearchParams } from "next/navigation";
import { Button, TextField } from "@mui/material";

import { вСловозміниЗВузомЗапит } from "./запити";
import ZaholovokСторінки from "@/складники/спільне/заголовок-сторінки";

function хухритиКлючіСловозміни(ключі: string[]) {
  ключі.sort((к1, к2) => (к1 === "ознаки" ? 1 : к2 === "ознаки" ? -1 : 0));
  return ключі;
}

const відбитВивідКлючівСловозміни: Record<string, string> = {
  частинаМови: "чм",
  знахіднийВид: "знахВид",
  доконаність: "док.",
  відмінок: "відм.",
};

export default function Holova() {
  const { ВипливаючеВікно, станВікна, назнПовідомлення } =
    useVyplyvaiucheVikno();

  const торник = useRouter();
  const відміриЗапиту = useSearchParams();

  const [слово, назнСлово] = useState<string>("");
  const [словоПоля, назнСловоПоля] = useState<string>("");
  const [многоСзЗВузом, назнМногоСзЗВузом] = useState<СловозмінаЗВузом[]>([]);

  useEffect(() => {
    (async function () {
      назнСлово(відміриЗапиту.get("слово") ?? "");
    })();
  }, [відміриЗапиту]);

  useEffect(() => {
    if (слово) назнСловоПоля(слово);
  }, [слово]);

  useEffect(() => {
    async function витяг() {
      try {
        if (слово) назнМногоСзЗВузом(await вСловозміниЗВузомЗапит(слово));
      } catch (e) {
        if (e instanceof Error) {
          назнПовідомлення(e.message);
          станВікна(true);
        } else {
          throw e;
        }
      }
    }
    витяг();
  }, [слово, назнПовідомлення, станВікна]);

  return (
    <>
      <ВипливаючеВікно />

      <ZaholovokСторінки заголовок="Словозмінення" />

      <div style={{ display: "flex" }}>
        <TextField
          id="outlined-multiline-flexible"
          label="Місце для слова..."
          value={словоПоля}
          // multiline
          maxRows={4}
          onChange={(под) => назнСловоПоля(под.target.value)}
        />
        <Button
          variant="outlined"
          onClick={(под) => {
            под.preventDefault();

            if (словоПоля) {
              const відміри = new URLSearchParams();
              відміри.set("слово", словоПоля);
              торник.push(`?${відміри.toString()}`);
            }
          }}
        >
          розібрати
        </Button>
      </div>
      <div>
        {многоСзЗВузом.map((сзЗВузом, вк) => {
          const словозміни = сзЗВузом.вуз.словозміни;
          const увузеніПоКлючах = увузитиПо(словозміни, (сз) =>
            хухритиКлючіСловозміни(Object.keys(сз.дані)).join(","),
          );
          return (
            <div
              key={сзЗВузом.сз.слово + вк}
              style={{ margin: "0px 0px 30px 0px" }}
            >
              <p>{сзЗВузом.сз.слово} </p>
              <hr />
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>
                  {[...увузеніПоКлючах.entries()].map(
                    ([ключіСтр, увСловозміни]) => {
                      const ключі = ключіСтр.split(
                        ",",
                      ) as (keyof ДаніСловозміни)[];
                      return (
                        <div
                          key={ключіСтр}
                          className="словозмінення-підщільник"
                        >
                          <div className="словозмінення-рядок">
                            <div className="словозмінення-стовпчик"></div>
                            {ключі.map((кл) => (
                              <div key={кл} className="словозмінення-стовпчик">
                                {відбитВивідКлючівСловозміни[кл] ?? кл}
                              </div>
                            ))}
                          </div>
                          {увСловозміни.map((сз, вк2) => {
                            return (
                              <div
                                className="словозмінення-рядок"
                                key={сз.слово + ключіСтр + вк2}
                              >
                                <div className="словозмінення-стовпчик">
                                  {сз.слово === слово ? (
                                    <b>{сз.слово}</b>
                                  ) : (
                                    сз.слово
                                  )}
                                </div>
                                {ключі.map((кл) => (
                                  <div
                                    key={кл}
                                    className="словозмінення-стовпчик"
                                  >
                                    {сз.дані[кл]}
                                  </div>
                                ))}
                              </div>
                            );
                          })}
                        </div>
                      );
                    },
                  )}
                </div>
                <pre style={{ maxHeight: "300px", overflow: "scroll" }}>
                  ({JSON.stringify(сзЗВузом.вуз.дані, null, 2)})
                </pre>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
}
