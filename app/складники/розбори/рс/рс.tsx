"use client";

import { РозбірСлова } from "@movnia/spilne";
import { ПервцевийРССкл } from "./первцевий-рс";

export function РССкл({ рс }: { рс: РозбірСлова }) {
  return (
    <div>
      {/* <p>{рс.слово}</p> */}
      <ПервцевийРССкл первцевийРС={рс.первцевийРС} />
    </div>
  );
}
