"use client";

import { ПервецьЗнак } from "@movnia/spilne";

export default function ВікноPidkazkaЗнак({ знак }: { знак: ПервецьЗнак }) {
  return (
    <div className="підказка-первець-щурб">
      <p className="підказка-первець-щурб-заголовок">Вид</p>
      <p className="підказка-первець-щурб-значення">
        <span className="text-stone-500">знак</span>
      </p>

      <p className="підказка-первець-щурб-заголовок">Знак</p>
      <p className="підказка-первець-щурб-значення">{знак.знак}</p>
    </div>
  );
}
