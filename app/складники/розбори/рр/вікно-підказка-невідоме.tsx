"use client";

export default function ВікноPidkazkaНевідоме() {
  return (
    <div className="підказка-первець-щурб">
      <p className="підказка-первець-щурб-заголовок">Вид</p>
      <p className="підказка-первець-щурб-значення">
        <span className="text-stone-500">невідомий</span>
      </p>
    </div>
  );
}
