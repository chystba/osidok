"use client";

import { Stack, Typography, useTheme } from "@mui/material";
import Image from "next/image";

export default function Zaholovok() {
  const theme = useTheme();
  return (
    <Stack direction="row" alignItems="center" spacing={2}>
      <Image
        width={40}
        height={40}
        src={
          theme.palette.mode === "dark" ? "/познака-світла.svg" : "/познака.svg"
        }
        alt="познака"
      />
      <Typography variant="h6">Мовня</Typography>
    </Stack>
  );
}
