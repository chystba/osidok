import {
  useState,
  Dispatch,
  SetStateAction,
  useCallback,
  useMemo,
} from "react";
import { Box, Modal, Typography } from "@mui/material";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const DemoModal = ({
  text,
  showDemoModal,
  setShowDemoModal,
}: {
  text: string;
  showDemoModal: boolean;
  setShowDemoModal: Dispatch<SetStateAction<boolean>>;
}) => {
  return (
    <Modal
      open={showDemoModal}
      onClose={() => setShowDemoModal(false)}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" component="h2">
          Чистьба
        </Typography>
        <Typography id="modal-modal-description" sx={{ mt: 2 }}>
          {text}
        </Typography>
      </Box>
    </Modal>
  );
};

export function useVyplyvaiucheVikno() {
  const [text, setText] = useState("");
  const [showDemoModal, setShowDemoModal] = useState(false);

  const ВипливаючеВікно = useCallback(() => {
    return (
      <DemoModal
        showDemoModal={showDemoModal}
        setShowDemoModal={setShowDemoModal}
        text={text}
      />
    );
  }, [showDemoModal, setShowDemoModal, text]);

  return useMemo(
    () => ({
      станВікна: setShowDemoModal,
      ВипливаючеВікно: ВипливаючеВікно,
      назнПовідомлення: setText,
    }),
    [setShowDemoModal, ВипливаючеВікно],
  );
}
