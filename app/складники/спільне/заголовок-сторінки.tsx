"use client";

import { Box, Typography } from "@mui/material";

export default function ZaholovokСторінки({
  заголовок,
}: {
  заголовок: string;
}) {
  return (
    <Box sx={{ mb: 4 }}>
      <Typography variant="h3">{заголовок}</Typography>
    </Box>
  );
}
