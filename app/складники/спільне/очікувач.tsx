"use client";

export default function Очікувач({
  чиОчікуєм,
  писДоОчікування,
}: {
  чиОчікуєм: boolean;
  писДоОчікування: string;
}) {
  return (
    <div className="sub-block">
      {чиОчікуєм ? (
        <p className="animate-pulse leading-10">
          {[
            "       ",
            "   ",
            "          ",
            "  ",
            "       ",
            "         ",
            "          ",
          ].map((с, вк) => {
            return (
              <span key={вк + с} className={`word`}>
                {с}
              </span>
            );
          })}
        </p>
      ) : (
        <p className="leading-10 text-slate-600">{писДоОчікування}</p>
      )}
    </div>
  );
}
