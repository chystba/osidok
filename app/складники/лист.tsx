import { ReactNode } from "react";
import Balancer from "react-wrap-balancer";

export default function Лист({
  title,
  demo,
  large,
}: {
  title: string;
  demo: ReactNode;
  large?: boolean;
}) {
  return (
    <div
      className={`relative col-span-1 h-fit overflow-hidden rounded-xl border border-gray-200 bg-white shadow-md ${
        large ? "md:col-span-2" : ""
      }`}
    >
      <div className="mx-5 mb-2 mt-4 text-center">
        <h2 className="bg-gradient-to-br from-black to-stone-500 bg-clip-text font-display text-xl font-bold text-transparent">
          <Balancer>{title}</Balancer>
        </h2>
      </div>
      <div className="mx-10 flex items-center justify-center">{demo}</div>
    </div>
  );
}
