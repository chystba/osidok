"use client";

import { ПерекладРП } from "@movnia/spilne";

export function ПерекладРПСкл({ переклад }: { переклад: ПерекладРП }) {
  return <div>{переклад.перекладенийРП.письмо}</div>;
}
