"use server";

import { вРП } from "@movnia/rozbir-pysma";
import { перекластиРП } from "@movnia/chystba";

export async function перекластиПисьмоЗапит(письмо: string) {
  return перекластиРП(await вРП(письмо));
}
