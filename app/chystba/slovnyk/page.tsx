"use client";

import { useEffect, useState } from "react";
import { useVyplyvaiucheVikno } from "@/складники/спільне/випливаюче-вікно";

import { словникПерекладівПисЗапит } from "./запити";
import ZaholovokСторінки from "@/складники/спільне/заголовок-сторінки";

export default function Holova() {
  const { ВипливаючеВікно, станВікна, назнПовідомлення } =
    useVyplyvaiucheVikno();

  const [словник, назнСловник] = useState<Awaited<
    ReturnType<typeof словникПерекладівПисЗапит>
  > | null>("");

  useEffect(() => {
    async function витяг() {
      try {
        назнСловник(await словникПерекладівПисЗапит());
      } catch (e) {
        if (e instanceof Error) {
          назнПовідомлення(e.message);
          станВікна(true);
        } else {
          throw e;
        }
      }
    }
    витяг();
  }, [назнПовідомлення, станВікна]);

  return (
    <>
      <ВипливаючеВікно />

      <ZaholovokСторінки заголовок="Чистьбовий словник" />

      {словник ? <pre style={{ overflowX: "scroll" }}>{словник}</pre> : null}
    </>
  );
}
