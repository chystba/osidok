"use client";

import { useEffect, useState } from "react";
import { useVyplyvaiucheVikno } from "@/складники/спільне/випливаюче-вікно";
import { useRouter, useSearchParams } from "next/navigation";
import { Box, Button, Divider, TextField, Typography } from "@mui/material";

import { вРПЗапит } from "./запити";
import ZaholovokСторінки from "@/складники/спільне/заголовок-сторінки";

export default function Holova() {
  const { ВипливаючеВікно, станВікна, назнПовідомлення } =
    useVyplyvaiucheVikno();

  const торник = useRouter();
  const відміриЗапиту = useSearchParams();

  const [письмо, назнПисьмо] = useState<string>("");
  const [письмоПоля, назнПисьмоПоля] = useState<string>("");
  const [рп, назнРП] = useState<Awaited<ReturnType<typeof вРПЗапит>> | null>(
    null,
  );

  useEffect(() => {
    (async function () {
      назнПисьмо(відміриЗапиту.get("письмо") ?? "");
    })();
  }, [відміриЗапиту]);

  useEffect(() => {
    if (письмо) назнПисьмоПоля(письмо);
  }, [письмо]);

  useEffect(() => {
    async function витяг() {
      try {
        if (письмо) назнРП(await вРПЗапит(письмо));
      } catch (e) {
        if (e instanceof Error) {
          назнПовідомлення(e.message);
          станВікна(true);
        } else {
          throw e;
        }
      }
    }
    витяг();
  }, [письмо, назнПовідомлення, станВікна]);

  return (
    <>
      <ВипливаючеВікно />

      <ZaholovokСторінки заголовок="РП" />

      <div style={{ display: "flex", width: "60%" }}>
        <TextField
          id="outlined-multiline-flexible"
          label="Місце для письма..."
          value={письмоПоля}
          multiline
          minRows={2}
          maxRows={6}
          fullWidth={true}
          onChange={(под) => назнПисьмоПоля(под.target.value)}
        />
        <Button
          variant="outlined"
          onClick={(под) => {
            под.preventDefault();

            if (письмоПоля) {
              const відміри = new URLSearchParams();
              відміри.set("письмо", письмоПоля);
              торник.push(`?${відміри.toString()}`);
            }
          }}
        >
          розібрати
        </Button>
      </div>
      <div>
        {рп ? (
          <>
            <Divider sx={{ mt: 1 }}>
              <Typography variant="h4">Первцевий РП</Typography>
            </Divider>
            {рп.первцевийРП.map((п, вк) => {
              return (
                <div key={п.стрічка + п.місцеПоч}>
                  {вк}. &#34;{п.стрічка}&#34; [{п.місцеПоч}]
                </div>
              );
            })}

            <Divider sx={{ mt: 1 }}>
              <Typography variant="h4">Реченевий РР</Typography>
            </Divider>
            {рп.реченевийРП.map((рр, вк) => {
              return (
                <div key={(рр.речення + вк)}>
                  - &#34;{рр.речення}&#34;{" "}
                  <pre
                    style={{
                      maxHeight: "300px",
                      width: "70%",
                      overflow: "scroll",
                    }}
                  >
                    {JSON.stringify(рр, null, 2)}
                  </pre>
                </div>
              );
            })}
          </>
        ) : null}
      </div>
    </>
  );
}
