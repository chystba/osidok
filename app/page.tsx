"use client";

import "./page.css";

import { useState } from "react";
import Balancer from "react-wrap-balancer";
import { useVyplyvaiucheVikno } from "app/складники/спільне/випливаюче-вікно";
import СтягувачКруг from "app/складники/спільне/стягувач-круг";
import Лист from "app/складники/лист";
import Очікувач from "./складники/спільне/очікувач";
import { ПерекладРП } from "@movnia/spilne";
import { РПСкл } from "./складники/розбори/рп/рп";
import ЗгортковийСкрип from "./складники/спільне/згортковий-скрип";
import { ПерекладРПСкл } from "./складники/переклад/переклад";
import { витягПерекладПисьма } from "./запити";

export default function Holova() {
  const [чиЧекаєм, назнЧиЧекаєм] = useState<boolean>(false);
  const [письмо, назнПисьмо] = useState<string>(
    "Система вогню. Велика метрика",
  );
  const [переклад, назнПереклад] = useState<ПерекладРП | undefined>(undefined);

  const { ВипливаючеВікно, станВікна, назнПовідомлення } =
    useVyplyvaiucheVikno();

  async function вйо() {
    if (!письмо || чиЧекаєм) return;

    назнЧиЧекаєм(true);
    try {
      назнПереклад(await витягПерекладПисьма(письмо));
    } catch (e) {
      if (e instanceof Error) {
        назнПовідомлення(e.message);
        станВікна(true);
      } else {
        throw e;
      }
    } finally {
      назнЧиЧекаєм(false);
    }
  }

  return (
    <>
      <div className="z-10 w-full max-w-xl px-5">
        <p
          className="mt-6 animate-fade-up text-center text-gray-500 opacity-0 md:text-xl"
          style={{ animationDelay: "0.25s", animationFillMode: "forwards" }}
        >
          <Balancer>Розбір та переклад мови</Balancer>
        </p>
      </div>
      <div className="my-10 grid w-10/12 grid-cols-3 px-5">
        <ВипливаючеВікно />
        <div className="grid w-full animate-fade-up grid-cols-1 gap-5 px-5">
          <Лист
            key="Письмо"
            title="Письмо"
            demo={
              <div className="mb-3 w-full pt-0">
                <textarea
                  id="message"
                  rows={10}
                  className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder-gray-400 dark:focus:border-blue-500 dark:focus:ring-blue-500"
                  placeholder="Місце для письма..."
                  value={письмо}
                  onChange={(под) => назнПисьмо(под.target.value)}
                ></textarea>
                <button
                  disabled={чиЧекаєм}
                  onClick={вйо}
                  className="group relative float-right mb-2 mr-2 mt-3 inline-flex items-center justify-center overflow-hidden rounded-lg bg-gradient-to-br from-cyan-500 to-blue-500 p-0.5 text-sm font-medium text-gray-900 hover:text-white focus:outline-none focus:ring-4 focus:ring-cyan-200 disabled:from-gray-500 disabled:to-gray-400 group-hover:from-cyan-500 group-hover:to-blue-500 dark:text-white dark:focus:ring-cyan-800"
                >
                  <span className="relative rounded-md bg-white px-5 py-2.5 transition-all duration-75 ease-in group-hover:bg-opacity-0 dark:bg-gray-900">
                    {чиЧекаєм ? <СтягувачКруг /> : "вйо"}
                  </span>
                </button>
              </div>
            }
            large={false}
          />
        </div>
        <div className="col-span-2">
          <ЗгортковийСкрип заголовок={<b>Переклад</b>}>
            <div className="w-full break-all">
              {переклад ? (
                <ПерекладРПСкл переклад={переклад} />
              ) : (
                <Очікувач
                  чиОчікуєм={чиЧекаєм}
                  писДоОчікування="Введіть письмо для перекладу..."
                />
              )}
            </div>
          </ЗгортковийСкрип>
          <ЗгортковийСкрип заголовок={<b>Розбір Перекладеного Письма</b>}>
            {переклад ? (
              <РПСкл рп={переклад.перекладенийРП} />
            ) : (
              <Очікувач
                чиОчікуєм={чиЧекаєм}
                писДоОчікування="Введіть письмо для розбору..."
              />
            )}
          </ЗгортковийСкрип>
          <ЗгортковийСкрип заголовок={<b>Розбір Первинного Письма</b>}>
            {переклад ? (
              <РПСкл рп={переклад.рп} />
            ) : (
              <Очікувач
                чиОчікуєм={чиЧекаєм}
                писДоОчікування="Введіть письмо для розбору..."
              />
            )}
          </ЗгортковийСкрип>
        </div>
      </div>
    </>
  );
}
