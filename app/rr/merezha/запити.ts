"use server";

import { вПервцевийРП } from "@movnia/rozbir-pysma";
import { вПервцевіРР, вЙмМережіJSON } from "@movnia/rozbir-rechennia";

export async function витягЙмМережаJSON(письмо: string) {
  const пРП = вПервцевийРП(письмо);
  if (пРП.length !== 1)
    throw new Error("Пис містить більше одного речення або жодного");

  const { первцевийРР, первцевоЩурбнийРР } = вПервцевіРР(пРП[0]!.стрічка);

  return await вЙмМережіJSON({ первцевийРР, первцевоЩурбнийРР }, 2);
}
