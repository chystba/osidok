"use client";

import * as d3 from "d3";
import { useEffect, useRef, useState } from "react";
import "./merezha.css";
import { useDebounce } from "use-debounce";
import { useVyplyvaiucheVikno } from "@/складники/спільне/випливаюче-вікно";
import {
  Box,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
  TextField,
  useTheme,
} from "@mui/material";
import { витягЙмМережаJSON } from "./запити";

function стрВМережу(стр: string) {
  if (!стр) return null;
  try {
    return JSON.parse(стр) as {
      вершини: {
        ключ: string;
        назва: string;
        чиПоч: string;
        чиКін: string;
        рівень: number;
        довжина: number;
      }[];
      дуги: { з: string; до: string }[];
    };
  } catch (e) {
    return null;
  }
}

export default function Holova() {
  const { ВипливаючеВікно, станВікна, назнПовідомлення } =
    useVyplyvaiucheVikno();

  const svgRef = useRef(null);
  const [мережаСтр, назнМережаСтр] = useState<string>("");
  const [зуншенаМережаСтр, назнЗуншенаМережаСтр] = useState<string | null>("");
  const [початковаМережаСтр, назнПочатковаМережаСтр] = useState<string>("");
  const [кінцеваМережаСтр, назнКінцеваМережаСтр] = useState<string | null>("");
  const [вибірМережі, назнВибірМережі] = useState<"1" | "2" | "3">("2");
  const [речення, назнРечення] = useState<string>("привіт, як справи?");
  const [сила, назнСилу] = useState<number>(500);
  const [втореньСилДуг, назнВторенняСилДуг] = useState<number>(5);
  const [чиРозміщенняСилою, назнЧиРозміщенняСилою] = useState<boolean>(true);

  const [витриматеРечення] = useDebounce(речення, 1000);

  const снуть = useTheme();

  useEffect(() => {
    async function витяг() {
      try {
        const { початковаМережа, зуншенаМережа, кінцеваМережа } =
          await витягЙмМережаJSON(витриматеРечення);
        назнЗуншенаМережаСтр(зуншенаМережа);
        назнПочатковаМережаСтр(початковаМережа);
        назнКінцеваМережаСтр(кінцеваМережа);
      } catch (e) {
        if (e instanceof Error) {
          назнПовідомлення(e.message);
          станВікна(true);
        } else {
          throw e;
        }
      }
    }
    витяг();
  }, [витриматеРечення, назнПовідомлення, станВікна]);

  useEffect(() => {
    if (вибірМережі === "1") {
      назнМережаСтр(початковаМережаСтр);
    }
    if (вибірМережі === "2") {
      назнМережаСтр(зуншенаМережаСтр ?? "");
    }
    if (вибірМережі === "3") {
      назнМережаСтр(кінцеваМережаСтр ?? "");
    }
  }, [вибірМережі, кінцеваМережаСтр, зуншенаМережаСтр, початковаМережаСтр]);

  useEffect(() => {
    const мережа = стрВМережу(мережаСтр);
    if (!мережа) return;

    const остРівень = мережа.вершини.reduce(
      (остРів, в) => (в.рівень > остРів ? в.рівень : остРів),
      0,
    );
    const вершНаРівні = мережа.вершини.reduce((вершНаРівні, в) => {
      if (вершНаРівні[в.рівень] !== undefined) вершНаРівні[в.рівень]++;
      else вершНаРівні[в.рівень] = 1;

      return вершНаРівні;
    }, {} as Record<string, number>);
    const найбВершНаРівні = Object.values(вершНаРівні).reduce(
      (найб, в) => (в > найб ? в : найб),
      0,
    );
    const крокШирина = 150;
    const крокВисота = 50;

    const width = 1500;
    const height = 1000;

    const потВершНаРівні = structuredClone(вершНаРівні);
    const nodes = мережа.вершини.map((в, вк) => ({
      index: вк,
      data: в,
      fx: чиРозміщенняСилою ? undefined : -(width / 2) + крокШирина * в.рівень,
      fy: чиРозміщенняСилою
        ? undefined
        : крокВисота * (потВершНаРівні[в.рівень]-- - вершНаРівні[в.рівень] / 2),
    }));
    const links = мережа.дуги.map((d) => ({
      source: мережа.вершини.findIndex((n) => n.ключ === d.з),
      target: мережа.вершини.findIndex((n) => n.ключ === d.до),
    }));

    const мастьПруг = снуть.palette.mode === "light" ? "grey" : "white";

    const simulation = d3
      .forceSimulation(nodes)
      .force(
        "link",
        d3
          .forceLink(links)
          .id((d) => d.index!)
          .iterations(втореньСилДуг)
          .distance(100),
      )
      .force("charge", d3.forceManyBody().strength(-сила).distanceMax(1000));
    // .force("x", d3.forceX())
    // .force("y", d3.forceY());

    const svg = d3
      .select(svgRef.current)
      .attr("viewBox", [-width / 2, -height / 2, width, height])
      .attr("width", width)
      .attr("height", height)
      .attr("style", "max-width: 100%; height: auto; font: 12px sans-serif;");
    svg.selectAll("*").remove();

    const вікно = svg.append("g");

    const zoom = d3.zoom().on("zoom", (e) => {
      вікно.attr("transform", e.transform);
    });
    // @ts-ignore
    svg.call(zoom);

    // Per-type markers, as they don't inherit styles.
    вікно
      .append("defs")
      .selectAll("marker")
      .data(["1"])
      .join("marker")
      .attr("id", (d) => `arrow`)
      .attr("viewBox", "0 -5 10 10")
      .attr("refX", 15)
      .attr("refY", -0.5)
      .attr("markerWidth", 16)
      .attr("markerHeight", 16)
      .attr("orient", "auto")
      .append("path")
      .attr("fill", мастьПруг)
      .attr("d", "M0,-5L10,0L0,5");

    const link = вікно
      .append("g")
      .attr("fill", "none")
      .attr("stroke-width", 0.5)
      .selectAll("path")
      .data(links)
      .join("path")
      .attr("stroke", (d) => мастьПруг)
      .attr("marker-end", (d) => `url(${new URL(`#arrow`, location.href)})`);

    const node = вікно
      .append("g")
      .attr("fill", "currentColor")
      .attr("stroke-linecap", "round")
      .attr("stroke-linejoin", "round")
      .selectAll("g")
      .data(nodes)
      .join("g");

    node
      .append("circle")
      .attr("stroke", "white")
      .attr("fill", (n) =>
        n.data.чиПоч ? "green" : n.data.чиКін ? "red" : "black",
      )
      .attr("stroke-width", 1.5)
      .attr(
        "r",
        (n) =>
          (n.data.чиПоч ? 8 : n.data.чиКін ? 6 : 4) *
          (1 + (n.data.довжина - 1) * 0.5),
      );

    // вікно
    // .append("circle")
    // .attr("stroke", "white")
    // .attr("stroke-width", 1.5)
    // .attr("r", 4);

    node
      .append("text")
      .attr("x", (t) => -(t.data.назва.length / 2) * 5)
      .attr("y", -10)
      .text((d) => d.data.назва)
      .clone(true)
      .lower()
      .attr("fill", "none");

    simulation.on("tick", () => {
      link.attr("d", function linkArc(d: any) {
        const r = 0;
        return `
          M${d.source.x},${d.source.y}
          A${r},${r} 0 0,1 ${d.target.x},${d.target.y}
        `;
      });
      node.attr("transform", (d: any) => `translate(${d.x},${d.y})`);
    });

    // return Object.assign(svg.node(), {scales: {color}});
  }, [мережаСтр, сила, чиРозміщенняСилою, втореньСилДуг, снуть]);

  return (
    <>
      <ВипливаючеВікно />
      <Box
        sx={{ "& .MuiTextField-root": { m: 1 } }}
        style={{ display: "flex" }}
      >
        <Box style={{ display: "flex", flexDirection: "column" }}>
          <TextField
            multiline
            placeholder="Місце для речення..."
            value={речення}
            onChange={(под) => назнРечення(под.target.value)}
            rows={5}
          />

          <Box>
            Вибір мережі:
            <Radio
              checked={вибірМережі === "1"}
              value="1"
              name="radio-buttons"
              onChange={(под) =>
                (под.target as HTMLInputElement).checked
                  ? назнВибірМережі("1")
                  : null
              }
            />
            <Radio
              checked={вибірМережі === "2"}
              value="2"
              name="radio-buttons"
              onChange={(под) =>
                (под.target as HTMLInputElement).checked
                  ? назнВибірМережі("2")
                  : null
              }
            />
            <Radio
              checked={вибірМережі === "3"}
              value="3"
              name="radio-buttons"
              onChange={(под) =>
                (под.target as HTMLInputElement).checked
                  ? назнВибірМережі("3")
                  : null
              }
            />
          </Box>
        </Box>

        <Box style={{ display: "flex", flexDirection: "column" }}>
          <TextField
            id="outlined-number"
            label="Сила вершин"
            type="number"
            value={сила}
            onChange={(под) => назнСилу(parseFloat(под.target.value))}
          />

          <TextField
            id="outlined-number"
            label="Вторень сил дуг"
            type="number"
            value={втореньСилДуг}
            onChange={(под) => назнВторенняСилДуг(parseInt(под.target.value))}
          />

          <FormControlLabel
            control={
              <Checkbox
                checked={чиРозміщенняСилою}
                onChange={(под) => назнЧиРозміщенняСилою(!!под.target.checked)}
              />
            }
            label="Чи розміщення силою?"
          />
        </Box>

        <TextField
          id="message"
          multiline
          placeholder="Місце для письма..."
          value={мережаСтр}
          onChange={(под) => назнМережаСтр(под.target.value)}
          rows={5}
        />
      </Box>
      <svg ref={svgRef}></svg>
    </>
  );
}
