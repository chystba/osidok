"use client";

import { useEffect, useState } from "react";
import { useVyplyvaiucheVikno } from "@/складники/спільне/випливаюче-вікно";
import { useRouter, useSearchParams } from "next/navigation";
import { Box, Button, Divider, TextField, Typography } from "@mui/material";

import { вРРЗапит } from "./запити";
import ZaholovokСторінки from "@/складники/спільне/заголовок-сторінки";

export default function Holova() {
  const { ВипливаючеВікно, станВікна, назнПовідомлення } =
    useVyplyvaiucheVikno();

  const торник = useRouter();
  const відміриЗапиту = useSearchParams();

  const [речення, назнРечення] = useState<string>("");
  const [реченняПоля, назнРеченняПоля] = useState<string>("");
  const [рр, назнРР] = useState<Awaited<ReturnType<typeof вРРЗапит>> | null>(
    null,
  );

  useEffect(() => {
    (async function () {
      назнРечення(відміриЗапиту.get("речення") ?? "");
    })();
  }, [відміриЗапиту]);

  useEffect(() => {
    if (речення) назнРеченняПоля(речення);
  }, [речення]);

  useEffect(() => {
    async function витяг() {
      try {
        if (речення) назнРР(await вРРЗапит(речення));
      } catch (e) {
        if (e instanceof Error) {
          назнПовідомлення(e.message);
          станВікна(true);
        } else {
          throw e;
        }
      }
    }
    витяг();
  }, [речення, назнПовідомлення, станВікна]);

  return (
    <>
      <ВипливаючеВікно />

      <ZaholovokСторінки заголовок="РР" />

      <div style={{ display: "flex", width: "60%" }}>
        <TextField
          id="outlined-multiline-flexible"
          label="Місце для речення..."
          value={реченняПоля}
          multiline
          minRows={2}
          maxRows={6}
          fullWidth={true}
          onChange={(под) => назнРеченняПоля(под.target.value)}
        />
        <Button
          variant="outlined"
          onClick={(под) => {
            под.preventDefault();

            if (реченняПоля) {
              const відміри = new URLSearchParams();
              відміри.set("речення", реченняПоля);
              торник.push(`?${відміри.toString()}`);
            }
          }}
        >
          розібрати
        </Button>
      </div>
      <div>
        {рр ? (
          <>
            <Divider sx={{ mt: 1 }}>
              <Typography variant="h4">Первцевий РР</Typography>
            </Divider>
            {рр.первцевийРР.map((п, вк) => {
              return (
                <div key={п.стрічка + п.місцеПоч}>
                  {вк}. &#34;{п.стрічка}&#34; [{п.місцеПоч}]
                </div>
              );
            })}

            <Divider sx={{ mt: 1 }}>
              <Typography variant="h4">Первцевощурбий РР</Typography>
            </Divider>
            {Object.entries(рр.первцевоЩурбнийРР).map(([кл, п], вк) => {
              return (
                <div key={кл + вк}>
                  {кл}: &#34;{п.щурб}&#34; [{JSON.stringify(п)}]
                </div>
              );
            })}

            <Divider sx={{ mt: 1 }}>
              <Typography variant="h4">Ладинний РР</Typography>
            </Divider>
            <pre>{JSON.stringify(рр.ладиннийРР.сс, null, 2)}</pre>

            <Divider sx={{ mt: 1 }}>
              <Typography variant="h4">Чолонний РР</Typography>
            </Divider>
            <pre>{JSON.stringify(рр.чолоннийРР, null, 2)}</pre>

            <Divider sx={{ mt: 1 }}>
              <Typography variant="h4">Слівний РР</Typography>
            </Divider>
            {Object.entries(рр.слівнийРР).map(([кл, рс], вк) => {
              return (
                <div key={кл + рс.слово + вк}>
                  {кл}: &#34;{рс.слово}&#34;{" "}
                  <pre
                    style={{
                      maxHeight: "300px",
                      width: "70%",
                      overflow: "scroll",
                    }}
                  >
                    {JSON.stringify(рс, null, 2)}
                  </pre>
                </div>
              );
            })}

            <Divider sx={{ mt: 1 }}>
              <Typography variant="h4">Додатковий РР</Typography>
            </Divider>
            <pre>{JSON.stringify(рр.додатковийРР, null, 2)}</pre>
          </>
        ) : null}
      </div>
    </>
  );
}
