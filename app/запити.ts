"use server";

import { перекластиРП } from "@movnia/chystba";
import { вРП } from "@movnia/rozbir-pysma";

export async function витягПерекладПисьма(письмо: string) {
  const рп = await вРП(письмо, {
    чиВикорПатілоРечення: true,
  });
  const перекладРП = await перекластиРП(рп, {
    чиНехтуватиПомилками: true,
  });

  return перекладРП;
}
