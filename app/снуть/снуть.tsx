"use client";
import { createTheme } from "@mui/material/styles";
import { getDesignTokens } from "./основа";
import { navigationCustomizations } from "./уопрочення/путитва";
// import { inputsCustomizations } from './уопрочення/вводи';

// https://stackblitz.com/github/mui/toolpad/tree/master/examples/core/auth-nextjs-themed?file=app%2Flayout.tsx,theme%2Fcustomizations%2Ffeedback.tsx
const lightTheme = createTheme({
  ...getDesignTokens("light"),
  components: {
    // ...inputsCustomizations,
    //   ...dataDisplayCustomizations,
    //   ...feedbackCustomizations,
    ...navigationCustomizations,
  },
});
const darkTheme = createTheme({
  ...getDesignTokens("dark"),
  components: {
    // ...inputsCustomizations,
    //   ...dataDisplayCustomizations,
    //   ...feedbackCustomizations,
    ...navigationCustomizations,
  },
});

const theme = {
  light: lightTheme,
  dark: darkTheme,
};

export default theme;
