import * as React from "react";
import { NextAppProvider } from "@toolpad/core/nextjs";
import DescriptionIcon from "@mui/icons-material/Description";
import FolderIcon from "@mui/icons-material/Folder";
import { AppRouterCacheProvider } from "@mui/material-nextjs/v15-appRouter";
import type { Navigation } from "@toolpad/core/AppProvider";
import theme from "./снуть/снуть";
import { DashboardLayout, PageContainer } from "@toolpad/core";
import Zaholovok from "./складники/спільне/заголовок";

const NAVIGATION: Navigation = [
  {
    segment: encodeURIComponent("чистьба"),
    title: "Чистьба",
    icon: <FolderIcon />,
    children: [
      {
        title: "Чищення (переклад)",
        icon: <DescriptionIcon />,
      },
      {
        segment: encodeURIComponent("словник"),
        title: "Чистьбовий словник",
        icon: <DescriptionIcon />,
      },
    ],
  },
  {
    segment: encodeURIComponent("рп"),
    title: "РП",
    icon: <FolderIcon />,
    children: [
      {
        title: "Розбір письма на РР",
        icon: <DescriptionIcon />,
      },
    ],
  },
  {
    segment: encodeURIComponent("рр"),
    title: "РР",
    icon: <FolderIcon />,
    children: [
      {
        title: "Розбір речення на РС",
        icon: <DescriptionIcon />,
      },
      {
        segment: encodeURIComponent("мережа"),
        title: "Ймовірнісна мережа",
        icon: <DescriptionIcon />,
      },
    ],
  },
  {
    segment: encodeURIComponent("рс"),
    title: "РС",
    icon: <FolderIcon />,
    children: [
      {
        segment: encodeURIComponent("розбір"),
        title: "Розбір слова",
        icon: <DescriptionIcon />,
        pattern: `${encodeURIComponent("розбір")}{/:slovo}*`,
      },
      {
        segment: encodeURIComponent("словозмінення"),
        title: "Словозмінення",
        icon: <DescriptionIcon />,
      },
      {
        segment: encodeURIComponent("истослівя"),
        title: "Истослів'я",
        icon: <DescriptionIcon />,
      },
      {
        segment: encodeURIComponent("стрій-слова"),
        title: "Стрій слова",
        icon: <DescriptionIcon />,
      },
      {
        segment: encodeURIComponent("словотворення"),
        title: "Словотворення",
        icon: <DescriptionIcon />,
      },
      {
        segment: encodeURIComponent("тваровий-словник"),
        title: "Тваровий словник",
        icon: <DescriptionIcon />,
      },
      {
        segment: encodeURIComponent("товкний-словник"),
        title: "Товкний словник",
        icon: <DescriptionIcon />,
      },
      {
        segment: encodeURIComponent("частотний-словник"),
        title: "Частотний словник",
        icon: <DescriptionIcon />,
      },
    ],
  },
];

export default async function RootLayout({
  children,
}: Readonly<{ children: React.ReactNode }>) {
  return (
    <html lang="ua" data-toolpad-color-scheme="dark">
      <body>
        <AppRouterCacheProvider options={{ enableCssLayer: true }}>
          <NextAppProvider
            theme={theme}
            branding={{
              homeUrl: "/",
            }}
          >
            <DashboardLayout
              navigation={NAVIGATION}
              slots={{
                appTitle: Zaholovok,
              }}
            >
              <PageContainer>{children}</PageContainer>
            </DashboardLayout>
          </NextAppProvider>
        </AppRouterCacheProvider>
      </body>
    </html>
  );
}
