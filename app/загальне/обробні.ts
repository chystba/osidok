export function увузитиПо<О, К extends string>(
  ряд: О[],
  по: (об: О) => К,
): Map<К, О[]> {
  const map = new Map<К, О[]>();
  ряд.forEach((item) => {
    const key = по(item);
    const collection = map.get(key);
    if (!collection) {
      map.set(key, [item]);
    } else {
      collection.push(item);
    }
  });
  return map;
}
